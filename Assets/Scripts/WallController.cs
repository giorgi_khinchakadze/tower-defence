﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour
{
    public GameObject HealthBar;
    public float Health;
    public float HealthBarLength;

    public static WallController Instance { get; private set; }

    private float _currentHealth;


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        _currentHealth = Health;
        HealthBar.transform.localScale = new Vector3(0.5f, HealthBarLength, 0.5f);
    }

    public void OnHit(float hit)
    {
        _currentHealth -= hit;
        HealthBar.transform.localScale = new Vector3(0.5f,
            Mathf.Clamp(HealthBarLength * (_currentHealth / Health), 0.0f, HealthBarLength), 0.5f);

        if (_currentHealth <= 0)
        {
            GameLogic.Instance.OnGameOver();
        }
    }

    public void Heal()
    {
        _currentHealth = Health;
        HealthBar.transform.localScale = new Vector3(0.5f, HealthBarLength, 0.5f);
    }
}
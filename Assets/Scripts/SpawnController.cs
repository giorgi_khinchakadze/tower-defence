﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public static SpawnController Instance { get; private set; }

    public List<GameObject> SpawnLocations;


    private float _cooldown = 0;

    private int _toSpawn = 0;
    private float _maxCooldown = 0;
    private GameObject _what;


    private float _waveCooldown;
    private bool _spawn;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Update()
    {
        if (!_spawn)
        {
            _waveCooldown -= Time.deltaTime;
            if (_waveCooldown <= 0)
            {
                _spawn = true;
            }
        }
        else
        {
            if (_toSpawn > 0)
            {
                if (_cooldown <= 0.000000001)
                {
                    _toSpawn--;
                    _cooldown = Random.Range(0, _maxCooldown);
                    Instantiate(_what, SpawnLocations[_toSpawn % SpawnLocations.Count].transform);
                }
                else
                {
                    _cooldown -= Time.deltaTime;
                }
            }
        }

    }


    public void Spawn(int count, float cooldown, GameObject what, float waveCooldown)
    {
        _spawn = false;
        _waveCooldown = waveCooldown;

        _toSpawn = count;
        _maxCooldown = cooldown;
        _what = what;
    }
}
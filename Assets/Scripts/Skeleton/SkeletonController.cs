﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Skeleton;
using Assets.Scripts.Spells;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.AI;

public class SkeletonController : MonoBehaviour, IFreezable, IDamagable
{
    public float Cooldown;
    public float Power;
    public int Score;
    public float Health;


    public GameObject HealthBar;
    public Vector3 HealthBarDimensions;

    private NavMeshAgent _agent;
    private Animator _animator;


    private bool _attack = false;

    private float _currentCooldown = float.MaxValue;

    private float _currentHealth;

    private float _freezeCooldown = float.MinValue;
    private bool _isFrozen;


    public void Start()
    {
        _animator = GetComponent<Animator>();

        _agent = GetComponent<NavMeshAgent>();
        _agent.Warp(transform.position);

        _currentHealth = float.MaxValue;
    }

    public void OnResurrected()
    {
        var destination = GameLogic.Instance.AttackLocations[Random.Range(0, GameLogic.Instance.AttackLocations.Count)]
            .transform.position;

        _agent.destination = destination;

        _currentHealth = Health;
    }

    public void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Wall")
        {
            _agent.enabled = false;
            _animator.SetBool("Idle", true);
            _attack = true;
        }
    }


    public void AfterHit()
    {
        if (_agent.enabled) _agent.isStopped = false;
    }


    public void OnDeath()
    {
        _animator.SetBool("Death", true);
        _animator.speed = 1;
        _agent.enabled = false;
    }

    public void AfterDeath()
    {
        GameLogic.Instance.SkeletonsLeft -= 1;
        GameLogic.Instance.Score += Score;
        Destroy(gameObject);
    }

    public void OnAttack()
    {
        WallController.Instance.OnHit(Power);
    }

    public void Update()
    {
        if (_isFrozen)
        {
            if (_freezeCooldown >= 0.0f)
            {
                _freezeCooldown -= Time.deltaTime;
            }
            else
            {
                _isFrozen = false;
                _animator.speed = 1;
                if (_agent.enabled) _agent.isStopped = false;
            }
        }
        else
        {
            if (_attack)
            {
                if (_currentCooldown >= Cooldown)
                {
                    _animator.SetTrigger("Attack");
                    _currentCooldown = 0.0f;
                }
                else
                {
                    _currentCooldown += Time.deltaTime;
                }
            }
        }
    }

    public void Freeze(float duration)
    {
        _animator.speed = 0;
        _freezeCooldown = duration;
        _isFrozen = true;
        if (_agent.enabled) _agent.isStopped = true;
    }

    public void Damage(float amount)
    {
        _currentHealth -= amount;

        RefreshHealthBar();

        if (!_isFrozen) _animator.SetTrigger("Hit");

        if (_currentHealth <= 0) OnDeath();
        if (_agent.enabled) _agent.isStopped = true;
    }

    public void RefreshHealthBar()
    {
        var x = HealthBarDimensions.x;
        var y = Mathf.Clamp(HealthBarDimensions.y * _currentHealth / Health, 0.000000f, HealthBarDimensions.y);
        var z = HealthBarDimensions.z;

        HealthBar.transform.localScale = new Vector3(x, y, z);

        if (_currentHealth <= 0) HealthBar.SetActive(false);
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct Range
{
    public float Max;
    public float Min;
}

public class CameraController : MonoBehaviour
{
    public Range X;
    public Range Z;
    public Range Y;
    public float RotationSpeed;
    public float Speed;
    public float ZoomSpeed;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = transform.position +
                             transform.forward * Input.GetAxis("Vertical") * Time.deltaTime * Speed +
                             transform.right * Input.GetAxis("Horizontal") * Time.deltaTime * Speed + 
                             Vector3.down * Input.mouseScrollDelta.y * ZoomSpeed;

        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, X.Min, X.Max),
            Mathf.Clamp(transform.position.y, Y.Min, Y.Max),
            Mathf.Clamp(transform.position.z, Z.Min, Z.Max)
        );

        var direction =
            Input.GetKey(KeyCode.Q) ? -1 :
            Input.GetKey(KeyCode.E) ? 1 : 0;
        transform.Rotate(Vector3.up, RotationSpeed * direction);
    }
}
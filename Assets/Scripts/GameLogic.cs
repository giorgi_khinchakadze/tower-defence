﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour
{
    public int StartingSpawnCount;
    public float SpawnCountIncrease;
    public float WaveCooldown;

    public Text SkeletonsLefText;
    public Text CurrentWaveText;
    public Text ScoreText;

    public float MaxSpawnCooldown;
    public GameObject Skeleton;
    public List<GameObject> AttackLocations;


    public GameObject GamePanel;
    public GameObject GameOverPanel;
    public Text GameOverScoreText;
    public Text GameOverWaveText;

    public static GameLogic Instance { get; private set; }

    private int _currentWave = 0;


    private int _skeletonsLeft;

    public int SkeletonsLeft
    {
        get { return _skeletonsLeft; }
        set
        {
            _skeletonsLeft = value;
            SkeletonsLefText.text = value.ToString();
            if (value == 0)
            {
                StartNewWave(StartingSpawnCount = (int) (StartingSpawnCount * SpawnCountIncrease), WaveCooldown);
            }
        }
    }


    private int _score;

    public int Score
    {
        get { return _score; }
        set
        {
            _score = value;
            ScoreText.text = value.ToString();
        }
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        StartNewWave(StartingSpawnCount, 0);
    }

    void StartNewWave(int count, float cooldown)
    {
        SpawnController.Instance.Spawn(count, MaxSpawnCooldown, Skeleton, cooldown);
        WallController.Instance.Heal();
        SkeletonsLeft = count;
        CurrentWaveText.text = (++_currentWave).ToString();
    }


    public void OnGameOver()
    {
        GamePanel.SetActive(false);
        GameOverPanel.SetActive(true);
        GameOverScoreText.text = _score.ToString();
        GameOverWaveText.text = (_currentWave - 1).ToString();
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}